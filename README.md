# Presentation2

This is a small demonstration project for the purposes to provide example of ComponentFactory implementation, 
for creating dynamic components. The project was created on node 12.3.1, so use the nvm to
switch to 12.3.1

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Running
Run npm install, and npm start 

