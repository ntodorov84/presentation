import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertsHostComponent } from './alerts.component';
import { AlertPopupComponent } from './alert-popup.component';



@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AlertsHostComponent],
  entryComponents: [AlertPopupComponent],
  exports: [AlertsHostComponent]
})
export class AlertsModule { }
