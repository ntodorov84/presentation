
import {Component, ComponentFactory, ComponentRef, ComponentFactoryResolver, ViewContainerRef, ViewChild} from '@angular/core';
import {AlertPopupComponent} from './alert-popup.component';

@Component({
  selector: 'app-alerts',
  template: `
    <ng-template #alertContainer></ng-template>
    <button (click)="createComponent('success')">Create success alert</button>
    <button (click)="createComponent('danger')">Create danger alert</button>
  `,
})
export class AlertsHostComponent {
 @ViewChild('alertContainer', { read: ViewContainerRef }) container: ViewContainerRef;
 componentRef: ComponentRef<any>;

  constructor(private resolver: ComponentFactoryResolver) {}

  createComponent(type) {
    this.container.clear();
    const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(AlertPopupComponent);

    this.componentRef = this.container.createComponent(factory);

    this.componentRef.instance.type = type;

  }

    ngOnDestroy() {
      if(this.componentRef) {
        this.componentRef.destroy();
      }

    }
}
