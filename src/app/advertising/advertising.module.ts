import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdService } from './ad.service';
import {AdBannerComponent} from './ad-banner.component'
import { HeroJobAdComponent } from './hero-job-ad.component';
import { HeroProfileComponent } from './hero-profile.component';
import { AdDirective } from './ad.directive';
import { AdvertisingComponent } from './advertising.component';



@NgModule({
  declarations: [
    AdvertisingComponent,
    AdBannerComponent,
    HeroJobAdComponent,
    HeroProfileComponent,
    AdDirective ],
  imports: [CommonModule],
  providers: [AdService],
  exports: [AdvertisingComponent],
  entryComponents: [ HeroJobAdComponent, HeroProfileComponent ],
})
export class AdvertisingModule { }
