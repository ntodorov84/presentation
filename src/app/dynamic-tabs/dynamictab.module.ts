import { PeopleListComponent } from './people/people-list.component';
import { PersonEditComponent } from './people/person-edit.component';
import { TabsComponent } from './tabs/tabs.component';
import { AppDynamictabComponent } from './dynamictab.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabComponent } from './tabs/tab.component';
import { DynamicTabsDirective } from './tabs/dynamic-tabs.directive';
import { ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    AppDynamictabComponent,
    TabsComponent,
    TabComponent,
    DynamicTabsDirective,
    PersonEditComponent,
    PeopleListComponent],
  imports: [
    CommonModule, ReactiveFormsModule
  ],
  exports: [AppDynamictabComponent],

})
export class AppDynamictabModule { }
