import { AdvertisingComponent } from './advertising/advertising.component';
import { AdvertisingModule } from './advertising/advertising.module';
import { AppDynamictabComponent } from './dynamic-tabs/dynamictab.component';
import { AlertsHostComponent } from './alerts/alerts.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertsModule } from './alerts/alerts.module';
import { AppDynamictabModule } from './dynamic-tabs/dynamictab.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {path: '', redirectTo: 'alerts', pathMatch: 'full'},
  {path: 'alerts', component: AlertsHostComponent},
  {path: 'tabs', component: AppDynamictabComponent},
  {path: 'advertisement', component: AdvertisingComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    AppDynamictabModule,
    AlertsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AdvertisingModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
